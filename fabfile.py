# coding:utf-8

from fabric.api import run, env, sudo
from fabric.contrib.files import exists
import os.path

# - Configurar

PROJECT_NAME = 'spiralis'
APP_NAME = 'spiralis'

# - Fin


def _join(*args):
    return os.path.join(*args)


env.use_ssh_config = True
env.roledefs = {
    'prod': ['app2', 'app3'],
    'staging': ['staging-apps']
}

REPO_URL = 'git@bitbucket.org:t-mob/{}.git'.format(PROJECT_NAME)


PYTHON_BIN = 'python3.8.2'

APP_HOME_PATH = '/opt/tmob/app'
APP_PATH = _join(APP_HOME_PATH, APP_NAME)
CLONE_PATH = '/tmp/deploy_{}'.format(APP_NAME)
CONFIG_PATH = '/opt/tmob/config/{}.py'.format(APP_NAME)
GIT_PATH = _join(CLONE_PATH, APP_NAME)
VAR_PATH = _join('/opt/tmob/var', APP_NAME)
VIRTUALENV_PATH = _join('/opt/tmob/pyenv', APP_NAME)


def prepare_code(branch='master'):
    sudo('rm -rf {}'.format(CLONE_PATH))
    run('git clone -b {} {} {}'.format(branch, REPO_URL, CLONE_PATH))


def update_code():
    sudo('rm -rf {}'.format(APP_PATH))
    sudo('mv {} {}'.format(GIT_PATH, APP_HOME_PATH))
    sudo('chown djangoapp:djangoapp {} -R'.format(APP_PATH))
    check_config()
    run_migrations()
    deploy_static()


def deploy_static():
    django_exec = _join(VIRTUALENV_PATH, 'bin', 'python')
    sudo('{} {}/manage.py collectstatic --noinput --settings=settings.production'.format(
            django_exec,
            APP_PATH
        )
    )


def run_migrations():
    django_exec = _join(VIRTUALENV_PATH, 'bin', 'python')
    sudo('{} {}/manage.py migrate --settings=settings.production'.format(
            django_exec,
            APP_PATH
        )
    )


def check_config():
    if not exists(CONFIG_PATH, use_sudo=True):
        config_template = _join(APP_PATH, 'settings', 'prod.example.py')
        sudo('cp {} {}'.format(config_template, CONFIG_PATH))

    sudo('ln -sf {} {}'.format(
        CONFIG_PATH,
        _join(APP_PATH, 'settings', 'production.py')
    ))


def check_var():
    if not exists(VAR_PATH):
        sudo('mkdir {}'.format(VAR_PATH))

        sudo('mkdir {}'.format(_join(VAR_PATH, 'static')))
        sudo('mkdir {}'.format(_join(VAR_PATH, 'media')))

        sudo('chown gunicorn:gunicorn {} -R'.format(VAR_PATH))


def create_virtualenv():
    sudo('virtualenv -p {} {}'.format(PYTHON_BIN, VIRTUALENV_PATH),
         user='virtualenv')


def install_requirements():
    pip_exec = _join(VIRTUALENV_PATH, 'bin', 'pip')
    sudo('{} install -r {}/requirements.txt'.format(
            pip_exec,
            CLONE_PATH
        ),
        user='pyenv'
    )


def flush_virtualenv():
    if exists(VIRTUALENV_PATH, use_sudo=True):
        sudo('rm -rf {}'.format(VIRTUALENV_PATH))

    #create_virtualenv()
    install_requirements()


def nginx_vhost():
    vhost = '/etc/nginx/vhosts/{}.conf'.format(APP_NAME)

    if env.roles[0] == 'staging':
        vhost_source = _join(CLONE_PATH, 'etc', 'nginx', 'staging.conf')

    elif env.roles[0] == 'prod':
        vhost_source = _join(CLONE_PATH, 'etc', 'nginx', 'prod.conf')

    sudo('cp {} {}'.format(vhost_source, vhost))


def gunicorn_ini():
    ini = '/etc/supervisor/conf.d/{}.ini'.format(APP_NAME)

    if env.roles[0] == 'staging':
        ini_source = _join(CLONE_PATH, 'etc', 'supervisor', 'staging.ini')

    elif env.roles[0] == 'prod':
        ini_source = _join(CLONE_PATH, 'etc', 'supervisor', 'prod.ini')

    sudo('cp {} {}'.format(ini_source, ini))


def app_start():
    sudo('supervisorctl start {}'.format(APP_NAME))


def app_stop():
    sudo('supervisorctl stop {}'.format(APP_NAME))


def app_restart():
    sudo('supervisorctl restart {}'.format(APP_NAME))
    sudo('supervisorctl restart spiralis_tasks:')


def app_install(branch='master'):
    prepare_code(branch)

    #create_virtualenv()
    install_requirements()

    update_code()

    check_config()
    check_var()

    nginx_vhost()
    gunicorn_ini()


def app_update(branch='master'):
    prepare_code(branch)
    update_code()
    app_restart()
