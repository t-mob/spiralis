import sys
import os
from project import root_join

sys.path.insert(0, root_join('apps'))

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'change_in_production'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']

LOGGER_NAME = 'spiralis.{}'

# Application definition
APPLICATION_IDENTIFIER = "T-mob/Spiralis"

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'billing',
    'landing',
    'main',
    'notifications',
    'subscriptions',
    'tracking',
    'host_consults',
    'action_listener',

    'core',
    'smanager',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',

    'tmob_audit.middleware.RequestIDMiddleware',
    'tmob_audit.middleware.RequestDataLogger',
    'tmob_audit.middleware.TimeStampMiddleware',
]

ROOT_URLCONF = 'project.urls'
# tmob_kit.middleware.RequestDataLogger
REQUESTDATALOGGER_URL_PREFIXES = [
    '/notifications',
    '/lp',
    '/mt',
    '/hoc',
]

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'project.wsgi.application'

DATABASE_ROUTERS = [
     'smanager.db_router.SubscriptionRouter',
#     'core.db_router.CoreRouter',
]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    },

    'tmob_core': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'tmob_core',
        'USER': 'root',
        'PASSWORD': '',
        'HOST': '127.0.0.1',
        'PORT': '3306',
        'OPTIONS': {
           'init_command': """SET default_storage_engine=MyISAM;
                              SET sql_mode='STRICT_TRANS_TABLES'""",
        },
    },

    'tmob_smanager': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'tmob_smanager',
        'USER': 'root',
        'PASSWORD': '',
        'HOST': '127.0.0.1',
        'PORT': '3306',
        'OPTIONS': {
           'init_command': """SET default_storage_engine=MyISAM;
                              SET sql_mode='STRICT_TRANS_TABLES'""",
        },
    },

}

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = False

MEDIA_ROOT = root_join('..', 'var', 'media')
MEDIA_URL = '/media/'

STATIC_ROOT = root_join('..', 'var', 'static')
STATIC_URL = '/static/'

CARRIER_ID = 37
MSISDN_LOCAL_FORMAT = "^9[0-9]{8}$"
MSISDN_PREFIX_INTERNATIONAL = "51"

CACHE_NOTIFIER = "AL_NOTIFIERS_SPIRALIS"
CACHE_PORTALS_PREFIX_KEY = "PORTALS_IN_CACHE"

SMANAGE_GET_INFO = "http://smanager.ti.t-mobs.com/code/get/"

PROVIDER_ID = "tmob"
PROVIDER_PASSWORD = "tm0bc0m1cs"

VERIFY_USER_STATUS_URL = "http://besubscription.com/BeSubscription/UserStatus"
SEND_SMS_URL = "http://besubscription.com/BeSubscription/SendSMS"
UNSUBSCRIBE_URL = "http://besubscription.com/BeSubscription/Unsubscribe"

TEST_TYPE_SMS_SEND_DEFAULT = 3

WELCOME_PAGE = "http://go.houseofcomics.app"
