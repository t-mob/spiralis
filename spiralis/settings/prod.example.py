from settings.base import *

DEBUG = False

ADMINS = (
     ('Support', 'support@t-mobs.com'),('Damian', 'dlacapra@gmail.com'),
)

MANAGERS = ADMINS
EMAIL_SUBJECT_PREFIX = '[Spiralis]'

SECRET_KEY = 'change_in_production'


CELERY_BROKER_URL = 'amqp://user:pass@rabbitmq2.ti.t-mobs.com//spiralis'

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': ['192.168.134.222:11211', '192.168.134.223:11211', ],
        'TIMEOUT': None,
        'KEY_PREFIX': 'spiralis_',
        'VERSION': 1,
    },
    "redis": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://redis.ti.t-mobs.com:6379/1",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
            "SOCKET_CONNECT_TIMEOUT": 5,  # in seconds
            "SOCKET_TIMEOUT": 5,  # in seconds
        }
    },
}

MEDIA_ROOT = '/opt/tmob/var/spiralis/media'
STATIC_ROOT = '/opt/tmob/var/spiralis/static'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'tmob_spiralis',
        'USER': '',
        'PASSWORD': '',
        'HOST': 'mysql.ti.t-mobs.com',
        'PORT': '3306',
        'OPTIONS': {
           'init_command': """SET default_storage_engine=MyISAM;
                              SET sql_mode='STRICT_TRANS_TABLES'""",
        },
    },

    'tmob_core': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'tmob_core',
        'USER': '',
        'PASSWORD': '',
        'HOST': 'mysql.ti.t-mobs.com',
        'PORT': '3306',
        'OPTIONS': {
           'init_command': """SET default_storage_engine=MyISAM;
                              SET sql_mode='STRICT_TRANS_TABLES'""",
        },
    },

    'tmob_smanager': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'tmob_smanager',
        'USER': '',
        'PASSWORD': '',
        'HOST': 'mysql.ti.t-mobs.com',
        'PORT': '3306',
        'OPTIONS': {
           'init_command': """SET default_storage_engine=MyISAM;
                              SET sql_mode='STRICT_TRANS_TABLES'""",
        },
    },
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,  # False,
    'formatters': {
        'standard': {
            'format': '%(name)s %(levelname)s %(message)s'
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'standard'
        },
        'syslog': {
            'class': 'logging.handlers.SysLogHandler',
            'formatter': 'standard',
            'facility': 'user',
            'address': '/dev/log',
        },
    },
    'loggers': {
        'spiralis': {
            'handlers': ['syslog'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'celery': {
            'handlers': ['syslog'],
            'level': 'DEBUG',
            'propagate': True,
        },
    }
}

import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

sentry_sdk.init(
    dsn="",
    integrations=[DjangoIntegration()],

    send_default_pii=True
)
