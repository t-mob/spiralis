from settings.base import *

# for celery
CELERY_ACCEPT_CONTENT = ['pickle', 'json',]
CELERY_TASK_SERIALIZER = 'pickle'
CELERY_RESULT_SERIALIZER = 'pickle'
CELERYBEAT_SCHEDULER = 'django_celery_beat.schedulers:DatabaseScheduler'

CELERY_BROKER_URL = 'amqp://guest:guest@localhost//spiralis'

#
# INSTALLED_APPS += [
#     'apps.billing',
#     'apps.landing',
#     'apps.main',
#     'apps.notifications',
#     'apps.subscriptions',
#     'apps.tracking',
#     'apps.host_consults',
# ]


LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,  # False,
    'formatters': {
        'standard': {
            'format': '%(name)s %(levelname)s %(message)s'
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'standard'
        },
    },
    'loggers': {
        'spiralis': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'celery': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': True,
        },
    }
}


CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': 'unique-snowflake',
    }
}

ADPARTNERS_AVAILABLES_URL = "http://wap.ti.t-mobs.com/adpartners/availables/"
ADPARTNERS_AVAILABLES_INFO = "ADPARTNERS"
