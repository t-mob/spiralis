from django.conf import settings
from core.request_http import request_http
from tmob_audit.log import getLogger

logger = getLogger(settings.LOGGER_NAME.format(__name__))

def get_hash_subscriber(msisdn, extra):
    # get the hash_code 
    logger.debug("Request to SManager to get hash for user: %s" % msisdn, extra=extra)
    smanager_hashcode_url = 'http://smanager.ti.t-mobs.com/code/get/'
    smanager_parameters = {'msisdn': msisdn}
    logger.debug("Request URL: %s; Parameters: %s." %(smanager_hashcode_url, smanager_parameters), extra=extra)
    response = request_http(url=smanager_hashcode_url, parameters=smanager_parameters, accept_header='application/json',
                            user_agent=settings.APPLICATION_IDENTIFIER)
    logger.debug("Response SManager get hashcode: %s" %(response), extra=extra)
    if response['error'] or response['response']['error']: # ERROR
        return None
    if response['response']['status'] == "NO_VALID_USER": # NO VALID
        return None
    hash_code = response['response']['hash_code']
    return hash_code
