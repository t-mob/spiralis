# coding: utf-8


class SubscriptionRouter(object):
    def db_for_read(self, model, **hints):
        if model._meta.app_label == 'smanager':
            return 'tmob_smanager'
        return None

    def db_for_write(self, model, **hints):
        if model._meta.app_label == 'smanager':
            return 'tmob_smanager'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        if 'smanager' in (obj1._meta.app_label, obj2._meta.app_label):
            return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        if app_label == 'smanager':
            return db == 'tmob_smanager'
        return None