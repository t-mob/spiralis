# coding: utf-8

from django.db import models
from core.models import Carrier
from django.utils.translation import ugettext as _

class Subscriber(models.Model):
    class Meta:
        verbose_name_plural = 'Subscribers'
        db_table = 'smanager_subscriber'
        unique_together = ('msisdn', 'carrier')

    def carrier_name(self):
        try:
            return Carrier.objects.get(pk=self.carrier).show_name
        except:
            return u'None'
        
    def __unicode__(self):
        return u'%s (%s)' % (self.msisdn, self.carrier_name())

    def __str__(self):
        return u'%s (%s)' % (self.msisdn, self.carrier_name())


    msisdn = models.CharField(_("MSISDN"), max_length=30, unique=True, db_index=True)
    carrier = models.IntegerField(_("Carrier"))
    hash_code = models.CharField(_("Hash Code"), max_length=30, blank=True, null=True)
    updated_at = models.DateTimeField(_("Updated At"), auto_now=True, editable=False)
    created_at = models.DateTimeField(_("Created At"), auto_now_add=True)


class Application(models.Model):
    class Meta:
        verbose_name_plural = 'Applications'
        db_table = 'smanager_application'

    def __unicode__(self):
        return u'({}) {}'.format(self.id, self.name)

    name = models.CharField("Name", max_length=100, unique=True)
    active = models.BooleanField("Active", default=True)


class Service(models.Model):
    class Meta:
        verbose_name_plural = 'Services'
        db_table = 'smanager_service'
        unique_together = ('name', 'application')
        ordering = ['application', 'show_name']

    def __unicode__(self):
        return u'{} ({}) [ App: {} ({}) ]'.format(
            self.show_name,
            self.id,
            self.application.name,
            self.application.id
        )

    def __str__(self):
        return u'{} ({}) [ App: {} ({}) ]'.format(
            self.show_name,
            self.id,
            self.application.name,
            self.application.id
        )

    name = models.CharField("Name", max_length=50)
    application = models.ForeignKey('Application', on_delete=models.CASCADE)
    show_name = models.CharField("Show Name", max_length=100)
    cross_selling = models.CharField("Cross Selling", max_length=50,
                                     blank=True, null=True)
    active = models.BooleanField("Active", default=True)
    updated_at = models.DateTimeField("Updated At", auto_now=True)
    created_at = models.DateTimeField("Created At", auto_now_add=True)


class Subscription(models.Model):
    class Meta:
        verbose_name_plural = 'Subscriptions'
        db_table = 'smanager_subscription'
        #unique_together = ('subscriber', 'service')

    #from smanager.renovation.models import RenewCycle

    #subscriber = models.ForeignKey(Subscriber, db_index=True)
    #service = models.ForeignKey(Service, db_index=True)
    subscriber_id = models.IntegerField(_('Subscriber'), db_index=True)
    service_id = models.IntegerField(_('Service'), db_index=True)
    external_identifier = models.CharField(max_length=50, blank=True, null=True)
    external_service = models.CharField(max_length=50, blank=True, null=True)
    enabled = models.BooleanField(_("Enabled"), default=True)
    status_id = models.IntegerField(_('SubscriptionStatus'), db_index=True)
    #status = models.ForeignKey(SubscriptionStatus, db_index=True)
    credits = models.IntegerField(_("Credits"), default=0)
    actived_at = models.DateTimeField(_("Activated At"),) # date active subscription
    origin_id = models.IntegerField(_('SubscriptionOrigin')) # Origin of Subscription, WAP WEB SMS...
    #origin = models.ForeignKey(SubscriptionOrigin) # Origin of Subscription, WAP WEB SMS...
    ad_partner = models.CharField(_("Ad Partner"), max_length=50, null=True)
    ad_partner_id = models.CharField(_("AdPartner Identifier"), max_length=200, null=True)
    ad_partner = models.CharField(_("Ad Partner"), max_length=50, null=True)
    cancelled_at = models.DateTimeField(_("Cancelled At"), null=True) # date cancel subscription
    author = models.CharField(_("Author"), max_length=50, null=True) # author of cancellation CSA..
    # Free attributes
    is_free = models.BooleanField(_("Is Free"), default=False)
    free_day = models.PositiveSmallIntegerField(_("Free Day"), default=0) # is a Free Subscription and the days of free
    free_to_date = models.DateTimeField(_("Free to Date"), null=True)
    free_next_check = models.DateTimeField(_("Next Free Check"), null=True)
    extra_info = models.CharField(_("Extra Info"), max_length=255, null=True)
    # Renew attributes
    next_renew = models.DateTimeField(_("Next Renew"), null=True)
    current_renew_cycle_id = models.IntegerField(_('RenewCycle'), null=True) 
    #current_renew_cycle = models.ForeignKey(RenewCycle, null=True) 
    updated_at = models.DateTimeField(_("Updated At"), auto_now=True, editable=False)
    created_at = models.DateTimeField(_("Created At"), auto_now_add=True)

