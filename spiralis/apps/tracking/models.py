# -*- coding: UTF-8 -*-

# Django Packages
from django.db import models

# Third Packages
from tmob_audit import AuditModel

# Own Packages
from landing.models import Portal

class Tracking(AuditModel):
    code_tracking = models.CharField(max_length=100, unique=True)
    application = models.ForeignKey(
        Portal,
        on_delete=models.CASCADE,
        related_name="track",
        related_query_name="track"
    )
    adpartner = models.CharField(max_length=60,
        blank=True,
        null=True
    )
    adpartner_id = models.CharField(max_length=60,
        blank=True,
        null=True
    )
    adpartner_publisher = models.CharField(max_length=60,
        blank=True,
        null=True
    )
    was_returned = models.BooleanField(default=False)
    when_was_returned = models.DateTimeField(blank=True, null=True)

    class Meta:
        ordering = ['created_at']
        verbose_name = ('track')
        verbose_name_plural = ('tracks')
        db_table = 'spiralis_tracking'

    def __str__(self):
        return f"{self.code_tracking}"
