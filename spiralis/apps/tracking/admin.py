from django.contrib import admin
from django.db import models
from django.forms import TextInput, Textarea, SelectMultiple

# Owner Packages
from .models import Tracking

@admin.register(Tracking)
class TrackingAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size':50})},
    }
    search_fields = ['code_tracking', 'adpartner', 'adpartner_id', 'adpartner_publisher', ]
    list_filter = ['application', 'adpartner', 'was_returned']
    list_display = ('code_tracking', 'application', 'adpartner', 'adpartner_id', 'adpartner_publisher', 'was_returned', 'created_at')
    date_hierarchy = 'created_at'
