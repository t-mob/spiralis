# Python Packages
import datetime

# Django Packages
from django.core.cache import cache
from django.conf import settings

# Third Packages
from celery import shared_task
# Owner Packages
from .models import Tracking
from landing.models import Portal
from tmob_audit.log import getLogger

logger = getLogger(settings.LOGGER_NAME.format(__name__))

@shared_task(bind=True, queue='tracking', max_retries=100)
def save_track(self, code_tracking, portal=None,
                adpartner=None, adpartner_id=None,
                adpartner_publisher=None, extra=None):
    try:
        #portal = Portal.objects.get(name=portal_name)
        track, created = Tracking.objects.get_or_create(code_tracking=code_tracking,
                                                        defaults={'application': portal }
                                                        )
        if created:
            track.adpartner = adpartner
            track.adpartner_id = adpartner_id
            track.adpartner_publisher = adpartner_publisher
            track.application = portal
        else:
            track.was_returned = True
            track.when_was_returned = datetime.datetime.now()
        track.save()
    except Exception as e:
        retry_number = save_track.request.retries
        logger.exception("Error save tracking: {}, retry #{}".format(code_tracking, retry_number), extra=extra)
        self.retry(exc=e, countdown=(60 * 5))
        #raise self.retry(exc=e, countdown=1)
