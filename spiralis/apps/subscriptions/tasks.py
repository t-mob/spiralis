# Django Packages
from django.conf import settings
from django.core.cache import cache
import logging

# Third Packages
from celery import shared_task
import datetime

# Owner Packages
from main.models import ServiceBinding
from billing.models import Billing
from billing.tasks import create_billing
from .models import RESUBSCRIBED, SUBSCRIBED, UNSUBSCRIBED, \
    SubscriptionTransaction, SubscriptionStatus
from tracking.tasks import save_track
from .states import verification_status
from tmob_audit.log import getLogger
from action_listener.tasks import queue_action_listener
from tracking.models import Tracking

logger = getLogger(settings.LOGGER_NAME.format(__name__))

@shared_task(bind=True, queue='subscription_transaction', max_retries=100)
def create_subscription_transaction(self, transaction_id, msisdn, service,
        created_in_spiralis, type_notification, status, channel_in,
        channel_out, keyword_in,keyword_out, fee, ad_provider, extra=None):
    try:
        service_ = ServiceBinding.objects.get(spiralis_service_id=service)
        date = get_date(created_in_spiralis)
        tracking = None
        if (tracking_lyst := Tracking.objects.filter(code_tracking=ad_provider)):
            tracking = tracking_lyst[0]
        subscription_transaction_ = SubscriptionTransaction(
            transaction_id=transaction_id,
            msisdn=msisdn,
            service=service,
            created_in_spiralis=date,
            type_notification=type_notification,
            status=status,
            channel_in=channel_in,
            channel_out=channel_out,
            keyword_in=keyword_in,
            keyword_out=keyword_out,
            fee=fee,
            ad_provider=ad_provider,
            tracking=tracking
        )

        success_operation = (status == 'Exitosa')

        resubscribed_condition = type_notification in [
            'Renovacion',
            'Cobro por contenido',
            'Periodo de retencion'
        ]
        cancelled_condition = type_notification in [
            'Cancelacion'
        ]
        subscribed_condition = type_notification in [
            'Suscripcion',
        ]

        # subscription
        subscription_status, created = SubscriptionStatus.objects.get_or_create(
            msisdn=msisdn,
            service_binding=service_
        )

        if created:
            slug_ = ('{}-{}'.format(msisdn, service_.identifier))
            subscription_status.slug = slug_

        if subscribed_condition:
            subscription_status.status = SUBSCRIBED
            subscription_status.tracking = tracking
            subscription_status.activate_at = datetime.datetime.now()
        elif resubscribed_condition:
            subscription_status.status = RESUBSCRIBED
            subscription_status.tracking = tracking
        elif cancelled_condition:
            subscription_status.status = UNSUBSCRIBED
            subscription_status.cancelled_at = datetime.datetime.now()
        subscription_status.save()

        queue_action_listener.delay(msisdn=msisdn,
                                    carrier=settings.CARRIER_ID,
                                    service_binding=service_,
                                    event=subscription_status.status,
                                    subscription=subscription_status,
                                    extra=extra)

        subscription_transaction_.subscription_status = subscription_status
        subscription_transaction_.save()

        # billing
        if (subscribed_condition or resubscribed_condition) and success_operation:
            logger.debug("notification {}, generate billing".format(type_notification), extra=extra)
            create_billing.delay(
                service,
                transaction_id,
                fee,
                type_notification,
                subscription_status.pk,
                extra=extra)

        # tracking
        # in cancel subscription do not receive ad_provider
        if ad_provider:
            save_track.delay(code_tracking=ad_provider)

    except Exception as e:
        logger.exception("subscription notification", extra=extra)
        raise self.retry(exc=e, countdown=(60 * 5))


def get_date(datetime_):
    try:
        year=int(datetime_[0:4])
        month=int(datetime_[4:6])
        day=int(datetime_[6:8])

        hours = 0
        minutes = 0
        seconds = 0

        if len(datetime_) >= 14:
            hours=int(datetime_[8:10])
            minutes=int(datetime_[10:12])
            seconds=int(datetime_[12:14])
        _datetime = datetime.datetime(year, month, day,
            hours, minutes, seconds)
    except:
        _datetime = datetime.datetime.now()
    finally:
        return _datetime


@shared_task(bind=True, queue='subscrition_status', max_retries=100)
def verify_subscritions_status(self, msisdn, status, service_id):
    try:
        service_ = ServiceBinding.objects.get(service_id=service_id)
        subscription_status = SubscriptionStatus.objects.get_or_create(
            msisdn=msisdn,
            service_binding=service_
        )

        _status = verification_status.get(int(status))

        subscription_status.status = _status
        subscription_status.save()
    except Exception as e:
        raise self.retry(exc=e, countdown=(60 * 5))
