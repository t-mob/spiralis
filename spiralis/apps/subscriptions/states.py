# UserStatus States

# Owner Packages
from .models import RESUBSCRIBED, SUBSCRIBED, UNSUBSCRIBED

verification_status = {
    1:SUBSCRIBED,   # Suscrito
    2:UNSUBSCRIBED, # Cancelado
    3:RESUBSCRIBED  # Retencion
}