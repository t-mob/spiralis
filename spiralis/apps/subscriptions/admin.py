# coding: utf-8

from django.contrib import admin
from django.utils.translation import ugettext as _
from django.db import models
from django.forms import TextInput
# Owner Packages
from .models import SubscriptionStatus, SubscriptionTransaction

@admin.register(SubscriptionTransaction)
class SubscriptionTransactionAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size':20})},
    }
    search_fields = ['msisdn', 'status', 'ad_provider', ]
    list_filter = ['type_notification', 'service', 'status', ]
    list_display = ('transaction_id', 'msisdn', 'service', 'status', 'type_notification', 'tracking', 'created_at', 'ad_provider', 'subscription_status')
    date_hierarchy = 'created_at'

@admin.register(SubscriptionStatus)
class SubscriptionStatusAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size':20})},
    }
    search_fields = ['msisdn', 'status', ]
    list_filter = ['status','service_binding',  ]
    list_display = ('msisdn', 'status', 'service_binding', 'tracking', 'activate_at', 'cancelled_at', 'created_at')
    date_hierarchy = 'activate_at'
