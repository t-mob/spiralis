# Django Packages
from django.db import models

# Third Packages
from tmob_audit.models import AuditModel

# Owner packages
from main.models import ServiceBinding
from tracking.models import Tracking

SUBSCRIBED = 'SUBSCRIBED'
UNSUBSCRIBED = 'UNSUBSCRIBED'
RESUBSCRIBED = 'RESUBSCRIBED'
NONE = 'NONE'


class SubscriptionStatus(AuditModel):
    SUBSCRIPTION_STATUS = [
        (SUBSCRIBED, 'Subscribed'),
        (UNSUBSCRIBED, 'Unsubscribed'),
        (RESUBSCRIBED, 'Resubscribed'),
        (NONE, 'None')
    ]

    status = models.CharField(
       max_length=30,
       choices=SUBSCRIPTION_STATUS,
       default=NONE
    )
    slug = models.CharField(max_length=40, null=True)
    service_binding = models.ForeignKey(
        ServiceBinding,
        on_delete=models.CASCADE)
    msisdn = models.CharField(max_length=30)
    activate_at = models.DateTimeField(null=True)
    cancelled_at = models.DateTimeField(null=True)
    tracking = models.ForeignKey(Tracking, on_delete=models.CASCADE, blank=True, null=True)

    class Meta:
        ordering = ["status"]
        verbose_name = ("Subscription Status")
        verbose_name_plural = ("Subscription's Status")
        db_table = 'spiralis_subscription_status'

    def __str__(self):
        return f"{self.slug}"


class SubscriptionTransaction(AuditModel):
    transaction_id = models.IntegerField()
    msisdn = models.CharField(max_length=30)
    service = models.PositiveIntegerField()
    created_in_spiralis = models.DateTimeField()
    type_notification = models.CharField(max_length=21)
    status = models.CharField(max_length=12)
    channel_in = models.CharField(
        max_length=30,
        null=True,
        blank=True)
    channel_out = models.CharField(
        max_length=30,
        null=True,
        blank=True)
    keyword_in = models.CharField(
        max_length=30,
        null=True,
        blank=True)
    keyword_out = models.CharField(
        max_length=30,
        null=True,
        blank=True)
    fee = models.FloatField(
        max_length=30,
        null=True,
        blank=True)
    ad_provider = models.CharField(
        max_length=30,
        null=True,
        blank=True)
    subscription_status = models.ForeignKey(
        SubscriptionStatus,
        on_delete=models.CASCADE,
        null=True,
        blank=True)
    tracking = models.ForeignKey(Tracking, on_delete=models.CASCADE, blank=True, null=True)

    class Meta:
        ordering = ["created_at"]
        verbose_name = ("Subscription Transaction")
        verbose_name_plural = ("Subscription's Transactions")
        db_table = 'spiralis_subscription_transaction'

    def __str__(self):
        return f"{self.transaction_id}"
