from django.apps import AppConfig


class HostConsultsConfig(AppConfig):
    name = 'host_consults'
