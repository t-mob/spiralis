# Django Packages
from django.http import HttpResponseNotAllowed, HttpResponseBadRequest, \
    HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt

# Third Packages
import requests

# Owner Packages
from subscriptions.tasks import verify_subscritions_status
from django.conf import settings
from landing.utils import get_portal
from .models import SMS_Sent, UnsubscribeUser
from main.models import ServiceBinding
from .tasks import send_sms_task
from tmob_audit.log import getLogger

logger = getLogger(settings.LOGGER_NAME.format(__name__))

PROVIDER_ID = settings.PROVIDER_ID
PROVIDER_PASSWORD = settings.PROVIDER_PASSWORD
VERIFY_USER_STATUS_URL = settings.VERIFY_USER_STATUS_URL
SEND_SMS_URL = settings.SEND_SMS_URL
TEST_TYPE_SMS_SEND_DEFAULT = settings.TEST_TYPE_SMS_SEND_DEFAULT
UNSUBSCRIBE_URL = settings.UNSUBSCRIBE_URL

def verify_user_status(request):
    http_response = HttpResponseNotAllowed("POST")
    if request.method == 'GET':

        params = request.GET
        portal_name = params.get('portal_name')
        spiralis_service_id = params.get('service_id')
        msisdn = params.get('msisdn')
        url = VERIFY_USER_STATUS_URL

        response = HttpResponse(status=403)
        if (service_binding := ServiceBinding.objects.get(spiralis_service_id=spiralis_service_id)):
            send_sms_task.delay(params, url)

        response = requests.post(url,
            data = {'providerId': PROVIDER_ID,
                    'password': PROVIDER_PASSWORD,
                    'serviceId': spiralis_service_id,
                    'msisdn': msisdn,
            }
        )

        http_response = HttpResponse(status=response.status_code, \
            content=response.reason)

        if response.ok:
            response_list = response.text.split('|')
            http_response = HttpResponse(status=200, content=response.text)
            if response_list == 3:
                status = response_list[0]
                verify_subscritions_status.delay(msisdn=msisdn,
                    status=status,
                    service_id=spiralis_service_id)

    return http_response

@csrf_exempt
def send_sms(request):
    extra = {'rid': request.rid}
    response = HttpResponse(status=405)

    if request.method == 'POST':
        params = request.POST
        logger.debug("receive parameters for send MT: {}".format(params), extra=extra)
        url = SEND_SMS_URL
        spiralis_service_id = params.get('service_id')
        try:
            if (service_binding := ServiceBinding.objects.get(spiralis_service_id=spiralis_service_id)):
                logger.debug("queue MT for service: {}".format(spiralis_service_id), extra=extra)
                send_sms_task.delay(params, extra=extra)
                response_text =  {'description': 'task was queued', 'error': False}
                response =  JsonResponse(response_text, status=200)
        except ServiceBinding.DoesNotExist:
            logger.warning("send MT for invalid service_id: {}".format(spiralis_service_id), extra=extra)
            response_text =  {'description': 'invalid service', "error": True}
            response =  JsonResponse(response_text, status=200)
    return response

@csrf_exempt
def unsubscribe_user(request):
    extra = {'rid': request.rid}
    if not request.method == 'GET':
        logger.debug("method not allowed", extra=extra)
        return HttpResponse(status=405)

    params = request.GET
    spiralis_service_id = params.get('service', None)
    msisdn = params.get('msisdn', None)

    if not msisdn or not spiralis_service_id:
        logger.debug("invalid requests unsubscription not allowed without parameters", extra=extra)
        return HttpResponse(status=400)

    try:
        if (service_binding := ServiceBinding.objects.get(spiralis_service_id=spiralis_service_id)):
            response = requests.post(UNSUBSCRIBE_URL, data = {'providerId': PROVIDER_ID,
                                                  'password': PROVIDER_PASSWORD,
                                                  'serviceId': spiralis_service_id,
                                                  'msisdn': msisdn, }
                                    )
            logger.debug("response spiralis unsubscription api: {}, code: {}".format(response.text, response.status_code), extra=extra)
            if response.ok:
                status, description = response.text.split('|')
                UnsubscribeUser.objects.create(
                    provider_id=PROVIDER_ID,
                    service_id=spiralis_service_id,
                    msisdn=msisdn,
                    code_response=status,
                    description_response=description
                )
                response_text =  {'description': description, "status": status, "error": False}
            else:
                response_text =  {'description': "error response ws", "status": response.status_code, "error": True}
    except ServiceBinding.DoesNotExist:
        logger.warning("invalid service_id: {} for unsubscription of msisdn: {}".format(spiralis_service_id, msisdn), extra=extra)
        response_text =  {'description': 'invalid service', "error": True}

    response =  JsonResponse(response_text, status=200)
    return response
