# coding: utf-8

from django.contrib import admin
from django.utils.translation import ugettext as _
from django.db import models
from django.forms import TextInput
# Owner Packages
from .models import SMS_Sent

@admin.register(SMS_Sent)
class SMS_SentAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size':40})},
        models.PositiveSmallIntegerField: {'widget': TextInput(attrs={'size':5})},
    }
    search_fields = ['transaction', 'message', 'destinations']
    list_filter = ['code_response', 'type_message']
    list_display = ('provider_id', 'service_id', 'destinations', 'message', 'type_message', 'code_response', 'description_response', 'transaction', 'updated_at', 'created_at')
