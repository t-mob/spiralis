# Django Packages
from django.conf import settings

# Third Packages
from celery import shared_task
import requests

# Owner Packages
from subscriptions.models import SubscriptionStatus
from .models import SMS_Sent
from main.models import ServiceBinding
from tmob_audit.log import getLogger
from django.conf import settings
from .exceptions import RequestIsNot200

PROVIDER_ID = settings.PROVIDER_ID
PROVIDER_PASSWORD = settings.PROVIDER_PASSWORD

VERIFY_USER_STATUS_URL = settings.VERIFY_USER_STATUS_URL
SEND_SMS_URL = settings.SEND_SMS_URL
TEST_TYPE_SMS_SEND_DEFAULT = settings.TEST_TYPE_SMS_SEND_DEFAULT
UNSUBSCRIBE_URL = settings.UNSUBSCRIBE_URL



logger = getLogger(settings.LOGGER_NAME.format(__name__))

@shared_task(bind=True, queue='mt', max_retries=100)
def send_sms_task(self, params, extra=None):
    try:
        url = settings.SEND_SMS_URL
        spiralis_service_id = params.get('service_id')
        destinations = params.get('destinations')
        transaction = params.get('transaction')
        message = params.get('message')

        response = requests.post(url,
            data = {'providerId': PROVIDER_ID,
                    'password': PROVIDER_PASSWORD,
                    'serviceId': spiralis_service_id,
                    'destinations': destinations,
                    'message': message,
                    'type': TEST_TYPE_SMS_SEND_DEFAULT
            }
        )

        if not response.ok:
            raise RequestIsNot200()

        response_list = response.text.split('|')
        status = response_list[0]
        description = response_list[1]

        SMS_Sent.objects.create(
            provider_id=PROVIDER_ID,
            service_id=spiralis_service_id,
            destinations=destinations,
            message=message,
            type_message=TEST_TYPE_SMS_SEND_DEFAULT,
            code_response=status,
            description_response=description,
            transaction=transaction
        )

    except Exception as e:
        logger.exception("sms sender", extra=extra)
        raise self.retry(exc=e, countdown=(60 * 5))
