# Generated by Django 3.0.6 on 2020-06-23 15:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('host_consults', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='sms_sent',
            name='transaction',
            field=models.CharField(default='', max_length=100),
        ),
    ]
