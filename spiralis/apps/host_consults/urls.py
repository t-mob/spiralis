# Django packages
from django.contrib import admin
from django.urls import path

# Owner packages
from .views import verify_user_status, unsubscribe_user, send_sms

urlpatterns = [
    path('user_status/', verify_user_status, name='user_status'),
    path('unsubscribe/', unsubscribe_user, name='unsubscribe_user'),
    path('send_sms/', send_sms, name='send_sms'),
]
