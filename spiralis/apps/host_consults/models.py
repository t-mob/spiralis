# Django Packages
from django.db import models

# Third Packages
from tmob_audit import AuditModel


class SMS_Sent(AuditModel):
    provider_id = models.CharField(max_length=50)
    service_id = models.CharField(max_length=50)
    destinations = models.TextField()
    message = models.CharField(max_length=255)
    type_message = models.CharField(max_length=50)
    code_response = models.CharField(max_length=10)
    description_response = models.CharField(max_length=100)
    transaction = models.CharField(max_length=100, default="")

    class Meta:
        ordering = ['created_at']
        verbose_name = ('SMS Sent')
        verbose_name_plural = ('SMS Sents')
        db_table = 'spiralis_mt'

    def __str__(self):
        return f"{self.message} [{self.created_at}]"


class UnsubscribeUser(AuditModel):
    provider_id = models.CharField(max_length=50)
    service_id = models.CharField(max_length=50)
    msisdn = models.CharField(max_length=50)
    code_response = models.CharField(max_length=10)
    description_response = models.CharField(max_length=100)

    class Meta:
        ordering = ['created_at']
        verbose_name = ('Unsubscriber User')
        verbose_name_plural = ('Unsubscriber Users')
        db_table = 'spiralis_unsubscription'

    def __str__(self):
        return f"{self.msisdn} [{self.created_at}]"
