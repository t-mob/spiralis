# Django Packages
from django.db import models

# Thrird Packages
from tmob_audit.models import AuditModel

# Owner Packages
from subscriptions.models import SubscriptionStatus
from main.models import ServiceBinding

class Billing(AuditModel):
    transaction_id = models.IntegerField()
    fee = models.FloatField(max_length=10)
    type_notification = models.CharField(max_length=50)
    subscription_status = models.ForeignKey(
        SubscriptionStatus,
        on_delete=models.CASCADE)
    service_binding = models.ForeignKey(
        ServiceBinding,
        on_delete= models.CASCADE,
        related_name='billings',
        related_query_name='billing',
        default=None
    )


    class Meta:
        ordering = ["created_at"]
        verbose_name = ("Billing")
        verbose_name_plural = ("Billings")
        db_table = 'spiralis_billing'

    def __str__(self):
        return f"{self.transaction_id}"
