# Django Packages
from django.conf import settings
# Third Packages
from celery import shared_task

# Owner Packages
from subscriptions.models import SubscriptionStatus
from .models import Billing
from main.models import ServiceBinding
from tmob_audit.log import getLogger
from action_listener.tasks import queue_action_listener

logger = getLogger(settings.LOGGER_NAME.format(__name__))

@shared_task(bind=True, queue='billing', max_retries=100)
def create_billing(self, service, transaction_id, fee,
        type_notification, notification_pk, extra=None):
    try:
        service_ = ServiceBinding.objects.get(spiralis_service_id=service)
        SubStatus = SubscriptionStatus.objects.get(pk=notification_pk)
        Billing_ = Billing(
            transaction_id=transaction_id,
            fee=fee,
            type_notification=type_notification,
            subscription_status=SubStatus,
            service_binding=service_
        )
        Billing_.save()

        queue_action_listener.delay(msisdn=SubStatus.msisdn, 
                                    carrier=settings.CARRIER_ID,
                                    service_binding=service_,
                                    event='BILLING',
                                    subscription=SubStatus,
                                    extra=extra)

    except Exception as e:
        logger.exception("new billing", extra=extra)
        raise self.retry(exc=e, countdown=(60 * 5))
