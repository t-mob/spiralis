from django.contrib import admin
from django.db import models
from django.forms import TextInput, Textarea, SelectMultiple

# Owner Packages
from .models import Billing

@admin.register(Billing)
class TrackingAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size':50})},
    }
    search_fields = ['transaction_id',  ]
    list_filter = ['type_notification', 'service_binding',]
    list_display = ('transaction_id', 'service_binding', 'type_notification', 'subscription_status', 'fee', 'created_at')
    date_hierarchy = 'created_at'
