# Generated by Django 3.0.6 on 2020-06-10 14:25

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0003_remove_servicebinding_portal'),
        ('billing', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='billing',
            name='service_binding',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, related_name='billings', related_query_name='billing', to='main.ServiceBinding'),
        ),
    ]
