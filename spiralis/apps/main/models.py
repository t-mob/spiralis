# Django packages
from django.db import models

# Third packages
from tmob_audit.models import AuditModel
from smanager.models import Service
# Owner packages

class ServiceBinding(AuditModel):
    identifier = models.CharField(max_length=50)
    spiralis_service_id = models.PositiveIntegerField(
        help_text='Id numerico dado por spiralis',
        default=0)
    tmob_service_id = models.PositiveIntegerField(
        help_text='Id numerico dado por tmob',
        default=0)
    is_enabled = models.BooleanField(default=True)
    price = models.DecimalField(max_digits=10, decimal_places=4, default=0)
    revenue = models.DecimalField(max_digits=10, decimal_places=4, default=0)

    class Meta:
        db_table = 'spiralis_service_binding'

    def __str__(self):
        return f'[{self.spiralis_service_id}] {self.identifier}'

    def service_name(self):
        return '{}'.format(Service.objects.get(pk=self.tmob_service_id))
