# coding: utf-8

from django import forms
from smanager.models import Service
from .models import ServiceBinding

class ServiceBindingForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ServiceBindingForm, self).__init__(*args, **kwargs)

        SERVICE_CHOICE = [('', '----')]
        for obj in Service.objects.all():
            SERVICE_CHOICE.append((obj.pk, '{}'.format(obj)))

        self.fields['tmob_service_id'] = forms.IntegerField(
            label='Service',
            widget=forms.Select(choices=SERVICE_CHOICE)
        )

    class Meta:
        model = ServiceBinding
        fields = '__all__'
