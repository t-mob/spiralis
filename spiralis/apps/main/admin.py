# coding: utf-8

from django.contrib import admin
from django.utils.translation import ugettext as _
from django.db import models
from django.forms import TextInput
# Owner Packages
from .models import ServiceBinding
from .forms import ServiceBindingForm

@admin.register(ServiceBinding)
class ServiceBindingAdmin(admin.ModelAdmin):
    form = ServiceBindingForm
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size':40})},
        models.PositiveSmallIntegerField: {'widget': TextInput(attrs={'size':5})},
    }
    search_fields = ['identifier', ]
    list_filter = ['is_enabled', ]
    list_editable = ('is_enabled',)
    list_display = ('identifier', 'spiralis_service_id', 'service_name', 'price', 'revenue', 'is_enabled', 'updated_at', 'created_at')
