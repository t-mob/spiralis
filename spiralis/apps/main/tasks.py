# Django Packages
from django.core.cache import cache

# Third Packages
from celery import shared_task
import logging

# Owner Packages

logger = logging.getLogger(__name__)


@shared_task(bind=True, queue='saver', max_retries=100)
def set_cache_host(self, host):
    try:
        cache.set('host', host)
    except Exception as e:
        #logger.exception(e)
        raise self.retry(exc=e, countdown=1)  