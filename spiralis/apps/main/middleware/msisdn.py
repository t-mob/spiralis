from django.core.cache import cache
from django.conf import settings

#Third packages
from tmob_audit.log import getLogger

logger = getLogger(settings.LOGGER_NAME.format(__name__))

#GalletitaIDMiddleware verifica si el usuario
#ya se ha identificado
class MsisdnIDMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        request.msisdn = None
                
        try: 
            request.msisdn = request.get_signed_cookie('msisdn', salt=settings.SALT_COOKIE)
        except:
            logger.info('No hay un cookie seteada en este usuario')

        response = self.get_response(request)

        return response

