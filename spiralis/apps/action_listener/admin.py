from django.contrib import admin
from django.utils.translation import ugettext as _
from django.db import models
from django.forms import TextInput, Textarea
from django.contrib import admin
from .models import Notifier


@admin.register(Notifier)
class NotifierAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.URLField: {'widget': TextInput(attrs={'size':100})},
        models.TextField: {'widget': Textarea(attrs={'rows':3, "cols":100})},
    }
    save_as = True
    list_display = ('service_binding', 'event', 'url', 'is_enabled')
    list_filter = ('event', 'is_enabled')
