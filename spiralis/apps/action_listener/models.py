from __future__ import unicode_literals
from tmob_audit import AuditModel
from django.db import models
from django.utils.translation import ugettext as _
from main.models import ServiceBinding
from subscriptions.models import RESUBSCRIBED, SUBSCRIBED, UNSUBSCRIBED

EVENT_CHOICES = [
    (SUBSCRIBED, 'Subscribed'),
    (UNSUBSCRIBED, 'Unsubscribed'),
    (RESUBSCRIBED, 'Resubscribed'),
    ('BILLING', 'Billing'),
]

class Notifier(AuditModel):
    class Meta:
        verbose_name_plural = _('notifiers')
        verbose_name = _('notifier')
        db_table = 'spiralis_action_listener_notifier'

    service_binding = models.ForeignKey(ServiceBinding, on_delete=models.CASCADE)
    event = models.CharField(max_length=50, choices=EVENT_CHOICES, db_index=True)
    url = models.URLField(max_length=200)
    params = models.TextField(
        help_text='In JSON format. Example: {"msisdn": "%(msisdn)s", "service_id": "%(service_id)s"}',
        blank=True,
        null=True
    )
    is_enabled = models.BooleanField(default=True)

    def __unicode__(self):
        return '{} {}'.format(self.service_binding, self.event)

    def __str__(self):
        return '{} {}'.format(self.service_binding, self.event)
