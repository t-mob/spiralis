# coding: utf-8
from django.conf import settings
from django.core.cache import cache
from tmob_audit.log import getLogger
from celery import shared_task, task
from .models import Notifier
from .views import reload_notifiers_in_cache
import requests
import simplejson

logger = getLogger(settings.LOGGER_NAME.format(__name__))

def process_notifier(msisdn, carrier, service_binding, event, subscription, extra=None):
    notifiers = cache.get(settings.CACHE_NOTIFIER)
    if not notifiers:
        notifiers = reload_notifiers_in_cache(extra=extra)

    if not event in notifiers:
        logger.info("no notifier for event {}".format(event), extra=extra)
        return None

    notifiers_for_event = notifiers[event]
    for notifier in notifiers_for_event:
        if notifier.service_binding == service_binding:
            parameters = {'msisdn': msisdn,
                          'service_id': service_binding.tmob_service_id,
                          'carrier_id': carrier,
                          'adpartner': '',
                          'adpartner_id': '',
                          'publisher': '', }

            if subscription and subscription.tracking:
                parameters['adpartner'] = subscription.tracking.adpartner
                parameters['adpartner_id'] = subscription.tracking.adpartner_id
                parameters['publisher'] = subscription.tracking.adpartner_publisher

            url = notifier.url % parameters
            url_parameters = notifier.params % parameters
            logger.info("make notify event: {} to url: {} params: {}".format(event, url, url_parameters), extra=extra)
            param_json = simplejson.loads(url_parameters)

            headers = {'content-type': 'text/html',
                       'accept': 'application/json',
                       'user-agent': settings.APPLICATION_IDENTIFIER, }
            r = requests.get(url, params=param_json, headers=headers)
            try:
                response_notification = r.json()
            except:
                response_notification = r.content
            logger.info("response notification: {}".format(response_notification), extra=extra)

@shared_task(queue='action_listener', ignore_result=True, max_retries=100)
def queue_action_listener(msisdn, carrier, service_binding, event, subscription, extra=None):
    try:
        process_notifier(msisdn, carrier, service_binding, event, subscription, extra=extra)
    except Exception as exc:
        retry_number = queue_action_listener.request.retries
        logger.exception("error in action listener msisdn {} retry #{}".format(msisdn, retry_number), extra=extra)
        queue_action_listener.retry(exc=exc, countdown=60)
