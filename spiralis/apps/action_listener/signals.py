# -*- coding: UTF-8 -*-

from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from tmob_audit.log import getLogger
from .models import Notifier
from .views import reload_notifiers_in_cache

logger = getLogger(settings.LOGGER_NAME.format(__name__))

@receiver(post_save, sender=Notifier)
def cache_service_reload(sender, instance, created, **kwargs):
    logger.info("reload cache for action_listener notifiers")
    reload_notifiers_in_cache()
