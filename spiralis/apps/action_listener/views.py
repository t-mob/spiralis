# -*- coding: UTF-8 -*-

from django.conf import settings
from django.core.cache import cache
from tmob_audit.log import getLogger
from .models import Notifier

logger = getLogger(settings.LOGGER_NAME.format(__name__))

def reload_notifiers_in_cache(extra=None):
    logger.info("reload notifier cache.", extra=extra)
    notifiers = Notifier.objects.filter(is_enabled=True).order_by('event')
    notifier_cache = {}
    for notifier in notifiers:
        if notifier.event in notifier_cache:
            notifier_cache[notifier.event].append(notifier)
        else:
            notifier_cache.update({ notifier.event : [notifier, ], })
    cache.set(settings.CACHE_NOTIFIER, notifier_cache)
    return notifier_cache