# -*- coding: UTF-8 -*-

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _

class ActionListenerConfig(AppConfig):
    name = 'action_listener'

    def ready(self):
        import action_listener.signals  # noqa