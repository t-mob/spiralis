from django.conf import settings
from django.core.cache import cache
from core.models import Carrier
import timeit
import re
from tmob_audit.log import getLogger

logger = getLogger(settings.LOGGER_NAME.format(__name__))

def loadCarrierCache():
    ftime = timeit.Timer()
    carrier_ids_cache = {}
    carrier_names_cache = {}
    try:
        carriers = Carrier.objects.all()
        for carrier in carriers:
            carrier_ids_cache.update({carrier.id: {'name': carrier.name, 'regexp': carrier.valid_msisdn, 'prefix': carrier.prefix_msisdn}})
            carrier_names_cache.update({carrier.name: carrier.id})
    except:
        logger.exception("Can not get all carrier from DB")
    cache.set(settings.CACHE_CARRIER_ID, carrier_ids_cache)
    cache.set(settings.CACHE_CARRIER_NAME, carrier_names_cache)
    logger.debug("CarrierIds: %s | CarrierNames %s." %(carrier_ids_cache, carrier_names_cache))
    logger.info("Done Reload Carrier info, ids and names. Time %s." % (str(ftime.timeit()) ))
    return carrier_ids_cache, carrier_names_cache

def isValidMsisdn(msisdn, carrier_id):
    carrier_cache = cache.get(settings.CACHE_CARRIER_ID)
    if not carrier_cache and not carrier_cache == []:
        carrier_cache, carrier_names_cache = loadCarrierCache()
        #carrier_cache = cache.get(settings.CACHE_CARRIER_ID)
    valid = False
    if carrier_cache:
        reg_exp = carrier_cache[long(carrier_id)]['regexp']
        pattern = re.compile(reg_exp)
        result = pattern.match(msisdn)
        logger.debug("is MSISDN %s valid: RegExp: %s - Result: %s" % (\
                      msisdn, reg_exp, result))
        return result
    return valid
