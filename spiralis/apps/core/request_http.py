from urllib import urlencode
from urllib2 import urlopen, Request, URLError
import simplejson
import httplib

class RequestHTTPEx(Exception):
    def __init__(self, desc, code=None):
        self.description = desc
        self.code = code

    def __str__(self):
        return repr(self.description)

def request_http(url, parameters, method='GET', content_type='text/html', accept_header=None, user_agent=None, success_code=[], encode=True, raw_data=None, headers=False, req_headers=()):
    DEFAULT_USER_AGENT = 'RequestHTTP-Python'
    response = {} 
    try:
        urlencode_parameters = None
        if parameters:
            if encode:
                urlencode_parameters = urlencode(parameters)
            else:
                urlencode_parameters = "&".join("%s=%s"%i for i in parameters.items())
        request = None
        if method == 'POST':
            if urlencode_parameters:
                request = Request(url, urlencode_parameters)
            elif raw_data:
                request = Request(url, raw_data)
            else:
                request = Request(url)
        else:
            if urlencode_parameters:
                request = Request("%s?%s" %(url, urlencode_parameters))
            else:
                request = Request(url)
        request.add_header('Content-Type', content_type)
        if accept_header:
            request.add_header('Accept', accept_header)
        if not user_agent:
            request.add_header('User-Agent', DEFAULT_USER_AGENT)
        else:
            request.add_header('User-Agent', user_agent)
        for req_h in req_headers:
            request.add_header(req_h[0], req_h[1])
        res = urlopen(request)
        payload = res.read()
        response['error'] = not (res.code == httplib.OK)
        if success_code:
            response['error'] = not (res.code in success_code)
        response['http_code'] = res.code
        if accept_header and 'application/json' in accept_header:
            response['response'] = simplejson.loads(payload)
        else:
            response['response'] = payload
        if headers:
            response['headers'] = res.info()
    except URLError, eu: 
        response['error'] = True
        if hasattr(eu, 'code'):
            response['http_code'] = eu.code
        else:
            response['http_code'] = None

        if hasattr(eu, 'reason'):
            response['response'] = str(eu.reason)
        else:
            response['response'] = str(eu)
    except Exception, e:
        response['error'] = True
        response['http_code'] = None
        response['response'] = str(e)
    return response
