from django.db import models
from django.utils.translation import ugettext as _

class Country(models.Model):
    class Meta:
        verbose_name_plural = 'Countries'
        db_table = 'core_country'

    def __unicode__(self):
        return u'%s (%s)' % (self.name, self.code)

    code = models.CharField("Code", max_length=2,
                            primary_key=True, unique=True)
    name = models.CharField("Name", max_length=255, unique=True)
    gmt = models.IntegerField("GMT", blank=False, null=False)
    updated_at = models.DateTimeField(auto_now=True, editable=False)
    created_at = models.DateTimeField(auto_now_add=True)

class Carrier(models.Model):
    class Meta:
        verbose_name_plural = 'Carriers'
        db_table = 'core_carrier'

    def __unicode__(self):
        return u'%s' % (self.show_name)

    def __str__(self):
        return u'%s' % (self.show_name)

    name = models.CharField("Name", max_length=50, unique=True,
                            db_index=True)
    show_name = models.CharField("Show Name", max_length=255, unique=True)
    valid_msisdn = models.CharField(
        "Validate MSISDN",
        max_length=255,
        help_text='RegEx that matchs with country\'s numbers')
    prefix_msisdn = models.CharField(
        "Prefix MSISDN",
        max_length=4,
        help_text='RegEx that matchs with country\'s numbers')
    country = models.ForeignKey(Country, on_delete=models.CASCADE)
    updated_at = models.DateTimeField(auto_now=True, editable=False)
    created_at = models.DateTimeField(auto_now_add=True)

class ShortCode(models.Model):
    class Meta:
        verbose_name_plural = 'ShortCodes'
        db_table = 'core_shortcode'
        unique_together = ('shortcode', 'carrier')

    def __unicode__(self):
        return u'%s (%s)' % (self.shortcode, self.carrier)

    def __str__(self):
        return u'%s (%s)' % (self.shortcode, self.carrier)

    BILLING_TYPE_CHOICE = ( ('FREE', 'FREE'),
                            ('MO', 'MO'),
                            ('MT', 'MT'), 
                            ('BILLING SYSTEM', 'BILLING SYSTEM'),
                          )

    shortcode = models.CharField(_("Short Code"), max_length=30, db_index=True)
    carrier = models.ForeignKey(Carrier, on_delete=models.CASCADE)
    active = models.BooleanField(_("Active"), default=True)
    billing_type = models.CharField(_("Billing Type"), max_length=20, choices=BILLING_TYPE_CHOICE, blank=False, null=False)
    price = models.DecimalField(_("Price"), max_digits=5, decimal_places=2, blank=True, null=True)
    revenue = models.DecimalField(_("Revenue"), max_digits=5, decimal_places=2, blank=True, null=True)
    created_at = models.DateTimeField(_("Created At"), auto_now=True, editable=False)    