type_ = {
    1:'Suscripcion',
    2:'Cancelacion',
    3:'Renovacion',
    4:'Cobro por contenido',
    5:'Periodo de retencion'
}

status_ = {
    1:'Exitosa', 
    0:'No Exitosa' 
}

channelin_ = {
    1:'WEB',
    2:'SMS',
    7:'WAP',
    128:'System',
    259:'SIM',        
}

channelout_ = {
    1:'WEB',
    2:'SMS',
    7:'WAP',
    128:'System'
}