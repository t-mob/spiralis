# Django packages
from django.contrib import admin
from django.urls import path

# Owner packages
from .views import notifications

# https://spiralis.t-mobs.com/notifications/receive
urlpatterns = [
    path('receive/', notifications, name='notifications'),
]
