# Django packages
from django.shortcuts import render
from django.http import HttpResponseBadRequest, \
    HttpResponse
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
import re

# Owner packages
from .states import type_, status_, \
    channelin_, channelout_
from subscriptions.tasks import create_subscription_transaction
from tmob_audit.log import getLogger

logger = getLogger(settings.LOGGER_NAME.format(__name__))

@csrf_exempt
def notifications(request):
    """Notifications received from spiralis

    Parameters
    ----------
    request: django.core.handlers.wsgi.WSGIRequest
        It's a simple request

    Query Parameters
    ----------------
    id: int
        Transtation identifier
    msisdn: char(11)
        identifier user account
    service: int
        identify service registrated in BeSubscription
    created: date(yyyyMMddHHmmss)
        date and time when the platform received the notification
    type: int
        type notification received
    status: int
        notification status
    channelIn: int
        channel throught which the user was registrated
        it exist only in subscriptions
    channelOut: int
        channel throught which the user was subscription cancelled
        it only exist in cancelleds
    keywordIn: varchar
        keyword send by the user.
        it only exists in SMT notifications and SMS channel
    keywordOut: varchar
        keyword send by the user to cancel your subscriptions
        it only exists in SMT notifications and SMS channel
    fee: int
        service fee
    adProvider: varchar
        Ad Network identifier
    """
    extra = {'rid': request.rid}
    if not (request.POST or request.GET):
        logger.warning("method HTTP {} not allowed.".format(request.method), extra=extra)
        return HttpResponseBadRequest()

    if not (params := request.POST):
        params = request.GET
    logger.debug("notification, data: {}".format(params), extra=extra)

    ip = request.META.get('HTTP_X_REAL_IP', '').strip()
    if not ip in ("3.134.186.173", ):
        logger.debug("notification, DISCARDED data: {}".format(params), extra=extra)
        return HttpResponse(status=200)

    transaction_id = params['id']
    msisdn = params['msisdn']
    service = int(params['service'])
    created_in_spiralis = params['created']
    type_notification = type_[int(params['type'])]
    status = status_[int(params['status'])]

    channel_in = params.get('channelIn')
    channel_out = params.get('channelOut')
    keyword_in = params.get('keywordIn')
    keyword_out = params.get('keywordOut')
    ad_provider = params.get('adProvider')
    if fee := params.get('fee'):
        fee = (int(fee))/100

    # Normalize MSISDN international format
    msisdn = msisdn.lstrip("+")
    if re.match(settings.MSISDN_LOCAL_FORMAT, msisdn) and not msisdn.startswith(settings.MSISDN_PREFIX_INTERNATIONAL):
        msisdn = "{}{}".format(settings.MSISDN_PREFIX_INTERNATIONAL, msisdn)

    create_subscription_transaction.delay(
        transaction_id,
        msisdn,
        service,
        created_in_spiralis,
        type_notification,
        status,
        channel_in,
        channel_out,
        keyword_in,
        keyword_out,
        fee,
        ad_provider,
        extra=extra
    )

    return HttpResponse(status=200)
