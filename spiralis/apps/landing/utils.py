# Django Packages
from django.conf import settings
from django.core.cache import cache

# Third Packages
import logging
import requests

# Owner Packages
from .models import Portal


logger = logging.getLogger(__name__)


def get_adpartner_availables(extra=None):
    """get a partner list

    Return
    ------
    list
        adpartner list with all data.
    """
    adpartners_info = cache.get(settings.ADPARTNERS_AVAILABLES_INFO)
    if not adpartners_info:
        response = requests.get(url=settings.ADPARTNERS_AVAILABLES_URL)
        adpartners_info = response.json()
        cache.set(settings.ADPARTNERS_AVAILABLES_INFO, adpartners_info)
    return adpartners_info


def get_adpartner_names(extra=None):
    """get and return all names of the paramaters"""
    name_list = [data.get('identifier') for data in get_adpartner_availables()]
    return name_list


def get_adpartner_parameter():
    """get and return all tracking of the parameters"""
    tracking_list = [data.get('tracking_parameter') for data in get_adpartner_availables()]
    return tracking_list


def get_adpartner_id(params):
    """get and return adpartner's tracking parameter"""
    for name in get_adpartner_parameter():
        if (tracking := params.get(name, None)):
            break
    return tracking


def get_portal(portal_name):
    """get the portal from the Database

    Parameters
    ----------
    portal_name: str
        Portal name

    Returns
    -------
    Portal object
        Return Portal if it exist or None if it does not exist.
    """
    portal = None
    key ="{}__{}".format(settings.CACHE_PORTALS_PREFIX_KEY, portal_name)
    if not (portal := cache.get(key)):
        try:
            portal = Portal.objects.get(active=True, name=portal_name)
        # if _portal:
        #     portal_dict = {
        #         'besubscription_url': _portal.url_redirect,
        #         'idlp': _portal.idlp,
        #         'besubscription_service_id': _portal.service_binding.spiralis_service_id,
        #         'portal_name': _portal.name
        #     }
            cache.set(key, portal)
        except Portal.DoesNotExist:
            return None
    return portal
