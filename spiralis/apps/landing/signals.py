# coding: utf-8

from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from tmob_audit.log import getLogger
from landing.models import Portal
from django.core.cache import cache

logger = getLogger(settings.LOGGER_NAME.format(__name__))

@receiver(post_save, sender=Portal)
def clear_cache(sender, instance, created, **kwargs):
    logger.debug("clear data cache portal")
    cache.delete('portal_data')
