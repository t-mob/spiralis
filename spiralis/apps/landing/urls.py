from django.urls import path
from .views import landing, confirmation

urlpatterns = [
    path('<str:portal_name>/confirmation/', confirmation, name='confirmation'),
    path('lp/<str:portal_name>/<str:adpartner>/', landing, name='landing'),

# el usuario entra a landing es redirigido a Spiralis.
# En Spiralis confirma el alta
# Spiralis redireccion al usuairo a confirmation
# La pagina confirmation  debe dar la informacion necesaria para el que usuario acceda al servicio/portal


]
