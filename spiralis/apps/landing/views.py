    # Django Packages
from django.shortcuts import render, reverse, redirect
from django.http import HttpResponseBadRequest, \
    HttpResponseNotAllowed, HttpResponseServerError
from django.conf import settings
from urllib.parse import urlencode
# Third Packages
import logging
import shortuuid
from tmob_audit.log import getLogger

# Owner Packages
from .utils import get_adpartner_id, get_portal, \
    get_adpartner_availables, get_adpartner_names, \
    get_adpartner_parameter
from tracking.tasks import save_track
from django.conf import settings
import requests
import re

logger = getLogger(settings.LOGGER_NAME.format(__name__))


def landing(request, portal_name, adpartner):
    """link where the users are redirect to the landing page.

    Parameters
    ----------
    request: django.core.handlers.wsgi.WSGIRequest
        It's a simple request
    portal_name: str
        Portal name where the users going to access.
    adpartner: str
        Partner who send us user's traffic.

    Query Parameters
    ----------------
    pub: str
        Publisher's number.

    some_parameter: str
        adpartner's tracking parameter.

    URL example
    -----------
    http://[URI]/[portal_name]/[adpartner]/?tm=clickid&pub=knddksd
    http://comics/headway/?extid=jsjsjsjsdjjfd&pub=32


    Return
    ------
    Redirect to the Landing page or HttpResponseBadRequest
    """
    extra = {'rid': request.rid}
    # log que adparner es y portal
    if not (portal:= get_portal(portal_name)):
        logger.warning('portal does not available with name: {}'.format(portal_name), extra=extra)
        return HttpResponseBadRequest()

    if not adpartner in get_adpartner_names():
        logger.warning('invalid value of adpartner: {}'.format(adpartner), extra=extra)
        return HttpResponseBadRequest()

    adpartner_id = get_adpartner_id(request.GET)
    adpartner_publisher = request.GET.get('pub', None)

    code_tracking = shortuuid.uuid()
    # log de la info del tracking
    logger.debug("tracking: {}, information adpartner: {}, adpartner_id: {}".format(code_tracking, adpartner, adpartner_id), extra=extra)
    save_track.delay(code_tracking=code_tracking,
                  portal=portal,
                  adpartner=adpartner,
                  adpartner_id=adpartner_id,
                  adpartner_publisher=adpartner_publisher,
                  extra=extra
    )

    _url = portal.url_redirect
    _service_id = "serviceId={}".format(portal.service_binding.spiralis_service_id)
    _idlp = "idLp={}".format(portal.idlp)
    _adprovider = "adProvider=" + code_tracking
    _user_agent = "userAgent=" + request.headers.get('User-Agent')

    query_params = [_service_id, _adprovider, _idlp, _user_agent]
    query = "&".join(query_params)
    redirect_url = "{}?{}".format(_url, query)
    logger.info("tracking: {}, go to url: {}".format(code_tracking, redirect_url), extra=extra)
    return redirect(redirect_url)

def confirmation(request, portal_name):

    #get info about subscription
    #hash = "cpe"
    msisdn = request.GET.get("msisdn", "")
    uid = request.GET.get("userId", "")
    channel = request.GET.get("ch", "")

    if msisdn:
        msisdn = msisdn.lstrip("+")
        if re.match(settings.MSISDN_LOCAL_FORMAT, msisdn) and not msisdn.startswith(settings.MSISDN_PREFIX_INTERNATIONAL):
            msisdn = "{}{}".format(settings.MSISDN_PREFIX_INTERNATIONAL, msisdn)
        # resp = requests.get(settings.SMANAGE_GET_INFO,
        #                     params={"msisdn": msisdn},
        #                     headers={"Accept": "application/json"})
        # resp_json = resp.json()
        # hash = resp_json.get("user_code", "cpe")
    p = { "msisdn": msisdn,
          "uid": uid,
          "ch": channel,
          "c": "CLARO_PE", }
    url_redirect = "{}?{}".format(settings.WELCOME_PAGE, urlencode(p))
    return redirect(url_redirect)
    #return render(request, 'landing/hoc/welcome.html', {"hash": hash})

def terms_hoc(request):
    return render(request, 'landing/hoc/terms.html')
