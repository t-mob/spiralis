# Django Packages
from django.contrib import admin
from django.db import models
from django.forms import TextInput, Textarea, SelectMultiple
# Owner Packages
from .models import Portal


@admin.register(Portal)
class PortalAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size':50})},
        models.TextField: {'widget': Textarea(attrs={'rows':3, "cols":100})},
        models.URLField: {'widget': TextInput(attrs={'size':90})},
        models.PositiveIntegerField: {'widget': TextInput(attrs={'size':5})},
    }
    list_filter = ['active']
    list_display = ('name', 'idlp', 'service_binding', 'url_redirect', 'active', 'created_at', 'updated_at')
    date_hierarchy = 'created_at'
