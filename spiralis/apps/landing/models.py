# -*- coding: UTF-8 -*-

# Django Packages

from django.db import models
from django.db.models.signals import post_save, pre_save
from django.core.cache import cache

# Third Packages
from tmob_audit import AuditModel
from main.models import ServiceBinding


class Portal(AuditModel):
    name = models.CharField(max_length=64)
    #code = models.CharField(max_length=50)
    description = models.TextField(blank=True, null=True)
    active = models.BooleanField(default=True)
    url_redirect = models.URLField(max_length=255,
        default="http://besubscription.com/BeSubscription/",
        help_text="URL where the users will be redirected. \
            suministred by the partner"
    )
    idlp = models.PositiveIntegerField(
        help_text="Landing Page ID. suministred by the partner"
    )
    service_binding = models.ForeignKey(
        ServiceBinding,
        on_delete=models.CASCADE,
        related_name='portals',
        related_query_name='portal',
        default=None
    )

    class Meta:
        ordering = ['active']
        verbose_name = ('portal')
        verbose_name_plural = ('portals')
        db_table = 'spiralis_portal'

    def __str__(self):
        return f"{self.name} [{self.service_binding}]"

#
# def clear_cache(sender, instance, **kwargs):
#     cache.delete('portal_data')
#
# post_save.connect(clear_cache, sender=Portal)
