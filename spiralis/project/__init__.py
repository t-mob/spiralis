# coding: utf-8

from os.path import join, abspath, dirname
from project.celery_app import app as celery_app
__all__ = ['celery_app']
#
#
# VERSION = '2020.02-24'
#
PROJECT_ROOT = join(abspath(dirname(__file__)), '..')


def root_join(*path):
    return join(abspath(PROJECT_ROOT), *path)
