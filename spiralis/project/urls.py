from django.contrib import admin
from django.urls import path, include
from landing.views import terms_hoc

urlpatterns = [
    path('terms/hoc/', terms_hoc), # HTML with terms and conditions

    path('admin/', admin.site.urls),
    path('notifications/', include('notifications.urls')),
    path('', include('landing.urls')),
    path('api/consults/', include('host_consults.urls')),



]

admin.site.site_header = 'Spiralis'
admin.site.site_title = 'Spiralis'
